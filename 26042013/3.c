/*
 *Korišćenjem programskog jezika C napisati Linux/UNIX program koji svom procesu detetu
 *korišćenjem redova poruka prosleđuje stringove koje prihvata sa tastature, a proces dete primljene stringove
 *upisuje u datoteku izlaz.txt. Komunikacija se prekida kada korisnik sa tastature unese reč KRAJ. (25 poena)
 */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <wait.h>

#define RED_PORUKA 10002
#define MAX_PORUKA 255

struct poruka
{
    long tip;
    char text[MAX_PORUKA];
};
int main(int argc, char *argv[])
{
    int msqid;
    struct poruka bafer;
    bafer.tip = 1;
    msqid = msgget(RED_PORUKA, IPC_CREAT | 0666);
    if(fork() == 0)
    {
        FILE* fajl;
        fajl = fopen("izlaz.txt", "w");
        if (fajl == NULL)
        {
            printf("Greska pri otvaranju fajla\n");
            exit(1);
        }
        while(strcmp(bafer.text, "KRAJ") != 0)
        {
            msgrcv(msqid, &bafer, MAX_PORUKA, 0, 0);
            fprintf(fajl, "%s\n", bafer.text);
        }
        fclose(fajl);
        printf("closed\n");
    }
    else
    {
        while(strcmp(bafer.text, "KRAJ") != 0)
        {
            scanf("%s", bafer.text);
            msgsnd(msqid, &bafer, MAX_PORUKA, 0);
        }
    }
    wait(NULL);
    msgctl(msqid, IPC_RMID, NULL);
    return 0;
}
