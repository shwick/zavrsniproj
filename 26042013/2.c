/*
 * Korišćenjem programskog jezika C kreirati dva Linudž procesa koja komuniciraju korišćenjem
 *datavoda. Jedan od procesa šalje drugom procesu slučajno generisanu celobrojnu vrednost. Drugim proces
 *primljenu vrednost štampa na standardnom izlazu. Nakon toga se komunikacija završava
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
    int pd[2];
    int pid;
    int buf;
    if(pipe(pd) < 0)
        exit(1);

    pid = fork();
    if(pid == 0)
    {
        close(pd[1]);
        read(pd[0], &buf, sizeof(buf));
        printf("%d\n", buf);
        close(pd[0]);
    }
    else
    {
        close(pd[0]);
        buf = rand();
        write(pd[1], &buf, sizeof(buf));
        close(pd[1]);
    }
    return(0);
}

