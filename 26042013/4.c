/*
 * Korišćenjem programskog jezika C napisati Linux program koji iz datoteke brojevi.dat čita niz
 *celobrojnih vrednosti i sve parne vrednosti smešta u deljivu memoriju definisanu ključem 15112, a sve
 *neparne vrednosti smešta u deljivu memoriju definisanu ključem 15678. U početnu lokaciju svakog objekta
 *deljive memorije treba upisati broj vrednosti koji je smešten u nastavku. Obezbediti da se oba objekta deljive
 *memorije kreiraju ukoliko ne postoje. (25 poena)
 */

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define PARNI_KLJUC 15112
#define NEPARNI_KLJUC 15678
void filewrite(int brojUpisa)
{
    FILE *fajl;
    fajl = fopen("brojevi.dat", "w");
    if(fajl == NULL)
    {
        printf("Fajl error\n");
        exit(1);
    }
    int i;
    int randBrojevi[brojUpisa];
    for(i = 0; i < brojUpisa; i++)
    {
        randBrojevi[i] = rand() %100;
    }
    fwrite(randBrojevi, sizeof(randBrojevi[0]), brojUpisa, fajl);
    fclose(fajl);
}
int main()
{
    int i;
    int velNiza = 30;
    int parMemId, neparMemId;
    int *shParna, *shNeparna;
    int brojevi[velNiza];

    //kreiram fajl iz kog uopste citam podatke
    filewrite(velNiza);
    FILE* fajl = fopen("brojevi.dat", "r");
    fread(brojevi, sizeof(brojevi[0]), velNiza, fajl);
    fclose(fajl);

    //velicina niza = velNiza * sizeof(int) + 1 jer u nultoj lokaciji cuvamo
    //broj parnih/neparnih elemenata, za redak slucaj da su svi parni ili neparni
    parMemId = shmget(PARNI_KLJUC, velNiza * sizeof(int) + 1, IPC_CREAT | 0666);
    neparMemId = shmget(NEPARNI_KLJUC, velNiza * sizeof(int) + 1, IPC_CREAT | 0666);
    if(parMemId < 0 || neparMemId < 0)
    {
        printf("Greska prilikom get deljive memorije\n");
        exit(1);
    }
    shParna = shmat(parMemId, NULL, 0);
    shNeparna = shmat(neparMemId, NULL, 0);
    if( shParna == NULL || shNeparna == NULL)
    {
        printf("Greska prilikom mapiranja deljive memorije\n");
        exit(1);
    }


    int brojParnih = 0;
    int brojNeparnih = 0;
    //prve lokacije cuvaju broj elemenata, indeksi elemenata krecu od 1
    shParna[0] = brojParnih;
    shNeparna[0] = brojNeparnih;
    for(i = 0; i < velNiza; i++)
    {
        if(brojevi[i] % 2 == 0)
        {
            brojParnih++;
            shParna[brojParnih] = brojevi[i];
            shParna[0] = brojParnih;
        }
        else
        {
            brojNeparnih++;
            shNeparna[brojNeparnih] = brojevi[i];
            shNeparna[0] = brojNeparnih;
        }
    }

    printf("Broj parnih: %d\n", shParna[0]);
    for(i = 1; i < brojParnih + 1; i++)
    {
        printf("%d\n", shParna[i]);
    }
    printf("Broj neparnih: %d\n", shNeparna[0]);
    for(i = 1; i < brojNeparnih + 1; i++)
    {
        printf("%d\n", shNeparna[i]);
    }
    shmdt(shParna);
    shmdt(shNeparna);
    shmctl(parMemId, IPC_RMID, 0);
    shmctl(neparMemId, IPC_RMID, 0);
    return 0;
}

