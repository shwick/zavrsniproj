/*
 * Korišćenjem programskog jezika C napisati Linux program u kome dve niti pristupaju deljivom
 *celobrojnom nizu. Prva nit periodično (na 3 s) slučajno odabranom elementu niza dodaje slučajno generisanu
 *celobrojnu vrednost iz opsega [-10 : 10]. Druga nit štampa elemente niza samo ukoliko je njihova sume (nakon
 *izmene od strane prve niti) paran broj. (25 poena)
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>

#define N 10

int niz[N];
pthread_mutex_t lock;
pthread_cond_t cond;

void * prva()
{
    while(true)
    {
        pthread_mutex_lock(&lock);

        int indexToChange = rand() % N;
        int valueToAdd = rand() % 10 - 20;
        niz[indexToChange] = niz[indexToChange] + valueToAdd;
        pthread_cond_signal(&cond);

        pthread_mutex_unlock(&lock);
        sleep(3);
    }
}

void * druga()
{
    while(true)
    {
        pthread_mutex_lock(&lock);
        pthread_cond_wait(&cond, &lock);
        int sum = 0;
        int i;
        for(i = 0; i < N; i++)
        {
            sum += niz[i];
        }
        if (sum % 2 == 0)
        {
            int i;
            for(i = 0; i < N; i++)
            {
                printf("%d ", niz[i]);
            }
            printf("suma = %d\n", sum);
        }
        pthread_mutex_unlock(&lock);
    }
}

int main(int argc, char *argv[])
{
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&cond, NULL);

    int i;
    for(i = 0; i < N; i++)
    {
        niz[i] = rand() % 100;
    }
    pthread_t nit1;
    pthread_t nit2;

    pthread_create(&nit1, NULL, (void*)prva, NULL);
    pthread_create(&nit2, NULL, (void*)druga, NULL);

    pthread_join(nit1, NULL);
    pthread_join(nit2, NULL);

    pthread_mutex_destroy(&lock);
    pthread_cond_destroy(&cond);

    return 0;
}
