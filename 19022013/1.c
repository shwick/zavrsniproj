/*
 *Korišćenjem programskog jezika C kreirati dva Linux procesa koji komuniciraju korišćenjem
 *datavoda. Prvi proces prihvata kao argument komandne linije jedan ceo broj. Taj broj korišćenjem datavoda
 *prosleđuje drugom procesu koji primljeni broj množi sa 2 i konačnu vrednost kao i originalni primljeni broj
 *štampa na ekranu.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char  *argv[])
{
    if(argc < 2)
    {
        printf("Nema argumenata na komandnoj liniji!\n");
        exit(1);
    }
    int pd[2];
    int pid;
    long buf;
    if(pipe(pd) < 0)
    {
        printf("Neuspesno kreiranje datavoda\n");
        exit(1);
    }

    pid = fork();
    if(pid > 0)
    {
       close(pd[0]);
       buf = strtol(argv[1], NULL, 10);
       write(pd[1], &buf, sizeof(buf));
       close(pd[1]);
    }
    else
    {
        close(pd[1]);
        read(pd[0], &buf, sizeof(buf));
        printf("Prvobitna vrednost: %ld\n", buf);
        printf("Dupla vrednost: %ld\n", buf * 2);
        close(pd[0]);
    }

    return 0;
}
