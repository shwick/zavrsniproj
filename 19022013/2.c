/*
 * Korišćenjem programskog jezika C napisati Linux program threadovi u kome dve niti u tekstualnu
 *datoteku upisuju redom cele brojeve od 1 do 20, pri čemu je svaki broj u zasebnom redu. Prva nit upisuje samo
 *neparne, a druga nit samo parne brojeve. Sinhronizacijom niti obezbediti da se brojevi upisuju u datoteku
 *pravilnim redosledom, 1 2 3 4 5 itd
 */
#include <stdio.h>
#include <pthread.h>

int niz[20];
pthread_mutex_t mutex;
pthread_mutex_t mutexPar;
pthread_mutex_t mutexNepar;
FILE *fajl;

void* parna()
{
    int j;
    for(j = 2; j <= 20; j += 2)
    {
        pthread_mutex_lock(&mutexPar);
        pthread_mutex_lock(&mutex);
        fprintf(fajl, "%d\n", j);
        pthread_mutex_unlock(&mutex);
        pthread_mutex_unlock(&mutexNepar);
    }
    return 0;
}

void* neparna()
{
    int i;
    for(i = 1; i < 20; i += 2)
    {
        pthread_mutex_lock(&mutexNepar);
        pthread_mutex_lock(&mutex);
        fprintf(fajl, "%d\n", i);
        pthread_mutex_unlock(&mutex);
        pthread_mutex_unlock(&mutexPar);
    }
    return 0;
}

int main(int argc, char *argv[])
{

    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_init(&mutexPar, NULL);
    pthread_mutex_init(&mutexNepar, NULL);

    pthread_mutex_lock(&mutexPar);

    fajl = fopen("parnepar.txt", "w");

    pthread_t nitPar;
    pthread_t nitNepar;

    pthread_create(&nitPar, NULL, (void*)parna, NULL);
    pthread_create(&nitNepar, NULL, (void*)neparna, NULL);

    pthread_join(nitPar, NULL);
    pthread_join(nitNepar, NULL);

    fclose(fajl);

    pthread_mutex_destroy(&mutex);
    pthread_mutex_destroy(&mutexPar);
    pthread_mutex_destroy(&mutexNepar);

    return 0;
}

