/*
 *Korišćenjem programskog jezika C napisati Linux program koji u zadatom direktorijumu (kao
 *ulazni argument programa zadaje se apsolutna putanja do direktorijuma) i njegovim poddirektorijumima
 *određuje i štampa imena svih regularnih datoteka koje su veće od 100KB takod a je dobijeni spisak sortiran po
 *veličini datoteke u rastućem redosledu.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>


#define MAX_NAME 1024
#define MAX_FILES 1024

//struct koristimo da bi lakse sortirali fajlove kad saznamo koji su
struct fajl
{
    char filename[MAX_NAME];
    int size;
};

struct fajl fajlovi[MAX_FILES];
int numFiles;

void processdir(char * path);

//funkcija za poredjenje se kasnije koristi u funkciji za sortiranje
int poredjenjeVelicine(const void *fajl1, const void *fajl2)
{
    struct fajl *f1 = (struct fajl*)fajl1;
    struct fajl *f2 = (struct fajl*)fajl2;
    return f1->size - f2->size;
}

int main(int argc, char * argv[])
{
	char dirname[MAX_NAME];
	struct stat statbuf;
    numFiles = 0; //broj fajlova koji zadovoljavaju kriterijum je prvobitno 0

	if (argc < 2)
	{
		printf("Nema dovoljno ulaznih argumenata\n");
		exit(-1);
	}

	strcpy(dirname, argv[1]);

	//proveravamo da li je uneta putanja validna
	if (stat(dirname, &statbuf) < 0)
	{
		printf("Doslo je do freske prilikom ocitavanja statusa direktorijuma\n");
		exit(-1);
	}

	//da li je uneta putanja do direktorijuma
	if (!S_ISDIR(statbuf.st_mode))
	{
		printf("Uneti argument ne predstavlja putanu do direktorijuma\n");
		exit(-1);
	}

	processdir(dirname);

    //gnu c funkcija za sortiranje. u terminalu mozete uneti man 3 qsort za dokumentaciju
    qsort(fajlovi, numFiles, sizeof(fajlovi[0]), poredjenjeVelicine);

    int i;
    for(i = 0; i < numFiles; i++)
    {
        printf("%d - %s\n", fajlovi[i].size, fajlovi[i].filename);
    }
	return 0;

}

void processdir(char * path)
{
	DIR * dp;
	struct dirent *dirp;
	char path1[MAX_NAME];
	struct stat statbuf;

	dp = opendir(path);

	if (dp == NULL)
	{
		printf("Greska prilikom otvaranja direktorijuma\n");
		exit(-1);
	}

	while ((dirp = readdir(dp)) != NULL)
	{
		if (strcmp(dirp->d_name, ".") == 0
			|| strcmp(dirp->d_name, "..") == 0)
			continue;

		strcpy(path1, path);
		strcat(path1, "/");
		strcat(path1, dirp->d_name);

		stat(path1, &statbuf);

		if (S_ISREG(statbuf.st_mode) && statbuf.st_size > 100 * 1024)
		{
            strcpy(fajlovi[numFiles].filename, path1);
            fajlovi[numFiles].size = statbuf.st_size;
            numFiles++;
		}
		else if (S_ISDIR(statbuf.st_mode))
			processdir(path1);
				
	}

	closedir(dp);

}
