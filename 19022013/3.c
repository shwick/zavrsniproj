/*
 * Korišćenjem programskog jezika C napisati Linux program koji omogućava da dva procesa
 *komuniciraju i sinhronizuju svoje izvršavanje korišćenjem signala. Prvi proces po slučajnom principu kreira
 *1024 celobrojne vrednosti i smešta ih u binarnu datoteku prenos.dat. Kada izgeneriše brojeve, obaveštava o
 *tome drugi proces, slanjem signala SIGUSR1 i pauzira svoje izvršavanje. Kada drugi proces primi signal on
 *sadržaj datoteke prenos.dat prikazuje na standardni izlazu, šalje prvom procesu signal SIGUSR2 i nakon toga
 *pauzira svoje izvršavanje. Po prijemu signala SIGUSR2, prvi proces ponovo startuje čitav postupak. Postupak
 *se ponavlja 4096 puta. Nakon toga se oba procesa završavaju.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#define BROJ_RANDOMS 100
#define BROJ_PONAVLJANJA 15

int randoms[BROJ_RANDOMS];

void genWrite()
{
    int j;
    FILE *fajl = fopen("prenos.dat", "w");
    for(j = 0; j < BROJ_RANDOMS; j++)
    {
        randoms[j] = rand() % 100;
    }
    fwrite(randoms, sizeof(int), BROJ_RANDOMS, fajl);
    fclose(fajl);
    printf("Parent generated/wrote\n");
    sleep(1);

}
void readWrite()
{
    int j;
    FILE *fajl = fopen("prenos.dat", "r");
    fread(randoms, sizeof(int), BROJ_RANDOMS, fajl);
    fclose(fajl);
    for(j = 0; j < BROJ_RANDOMS; j++)
    {
        printf("%d\n", randoms[j]);
    }
    printf("child printed\n");
    sleep(1);
}


int main(int argc, char *argv[])
{
    int i;
    int j;
    pid_t cpid;
    cpid = fork();
    if(cpid != 0)
    {
        signal(SIGUSR2, genWrite);
        genWrite();
        for(i = 0; i < BROJ_PONAVLJANJA; i++)
        {

            kill(cpid, SIGUSR1);
            /*printf("Parent wait, %d\n", i);*/
            pause();
            /*printf("Parent unpaused\n");*/
        }
        printf("parent exit\n");
    }
    else
    {
        pid_t parentPid = getppid();
        signal(SIGUSR1, readWrite);
        for(i = 0; i < BROJ_PONAVLJANJA; i++)
        {
            /*printf("child wait\n");*/
            pause();
            /*printf("child unpaused\n");*/
            kill(parentPid, SIGUSR2);
        }
        printf("child exit\n");
    }
    return 0;
}
