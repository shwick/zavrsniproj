/*
 * Korišćenjem programskog jezika C napisati Linux program koji nalazi maksimalni elemenat
 *celobrojnog niza proizvoljne dužine čiji se elementi unose sa tastature u glavnoj niti. Broj elemenata niza se
 *zadaje kao argument komandne linije. Za potrebe pronalaženja maksimuma, po završenom unosu elemenata
 *niza, pokreće se zasebna nit kojoj se kao argumenti prosleđuju broj elemenata niza i sam niz celih brojeva.
 *Izračunati maksimum nit štampa na standardni izlaz
 */
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

struct nizElem
{
    int *niz;
    int brElem;
};
void* max(void *nizElem)
{
    struct nizElem* thrNiz = (struct nizElem*) nizElem;
    int max = thrNiz->niz[0];
    int i;
    for(i = 1; i < thrNiz->brElem; i++)
    {
        if(thrNiz->niz[i] > max)
        {
            max = thrNiz->niz[i];
        }
    }
    printf("Najveci element je: %d\n", max);
    return 0;
}

int main(int argc, char *argv[])
{
    int i;
    if(argc != 2)
    {
        printf("Potreban jedan argument!\n");
        return 1;
    }
    struct nizElem *niz = malloc(sizeof(struct nizElem));
    niz->brElem = atoi(argv[1]);
    niz->niz = malloc(niz->brElem * sizeof(int));
    
    for(i = 0; i < niz->brElem; i++)
    {
        scanf("%d", &niz->niz[i]);
    }

    pthread_t nit;
    pthread_create(&nit, NULL, (void*)max, (void*)niz);
    pthread_join(nit, NULL);
    free(niz->niz);
    free(niz);
    return 0;
}
