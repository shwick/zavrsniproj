/*
 *Korišćenjem programskog jezika C napisati Linux program koji u zadatom direktorijumu (kao
 *ulazni argument programa zadaje se apsolutna putanja do direktorijuma) i njegovim poddirektorijumima
 *određuje i štampa imena svih regularnih datoteka koje su veće od 100KB
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>

void processdir(char *path);
int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        printf("Nema dovoljno argumenata!\n");
        return 1;
    }
    char dirname[strlen(argv[1])];
    struct stat statbuf;
    strcpy(dirname, argv[1]);
    if(stat(dirname, &statbuf)  < 0)
    {
        printf("Greska prilikom citanja statusa direktorijuma\n");
        return 1;
    }
    if(!S_ISDIR(statbuf.st_mode))
    {
        printf("Argument nije direktorijum!\n");
        return 1;
    }
    processdir(dirname);

    return 0;
}

void processdir(char *path)
{
    DIR *dp;
    struct dirent * dirp;
    char path1[1024];
    struct stat statbuf;

    dp = opendir(path);

    if(dp == NULL)
    {
        printf("Greska prilikom otvaranja direktorijuma!\n");
        exit(1);
    }
    while((dirp = readdir(dp)) != NULL)
    {
        if(strcmp(dirp->d_name, ".") == 0 || strcmp(dirp->d_name, "..") == 0)
            continue;
        strcpy(path1, path);
        strcat(path1, "/");
        strcat(path1, dirp->d_name);

        stat(path1, &statbuf);

        if(S_ISREG(statbuf.st_mode) && statbuf.st_size > 100 * 1024)
            printf("%s\n", path1);
        else if(S_ISDIR(statbuf.st_mode))
                processdir(path1);

    }
}
