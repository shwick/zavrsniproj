/*
 *Korišćenjem programskog jezika C napisati Linux program koji omogućava modifikaciju
 *standardnog izlaza bilo kog programa i snimanje standardnog izlaza u specificiranu datoteku. Naziv programa
 *koji se startuje i čiji se izlaz modifikuje se zadaje kao prvi argument komandne linije, a naziv tekstualne
 *datoteke u koju se upisuje izlaz kao drugi argument komandne linije. Standardni izlaz se modifikuje tako da se
 *svako pojavljivanje karaktera “b” zamenjuje karakterom “B”
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>

int main(int argc, char *argv[])
{
    int i;
    int pd[2];
    if(pipe(pd) < 0)
    {
        printf("Greska pri kreiranju pipea!\n");
        return 1;
    }
    if(fork() == 0)
    {
        close(pd[0]);
        dup2(pd[1], 1); //redirekcija izlaza
        //dup2(pd[1], STDOUT_FILENO); //moze i ovako, STDIN_FILENO bi bio ulaz

        execvp(argv[1], &argv[1]);
    }
    else
    {
        close(pd[1]);
        FILE *fajl = fopen("izlaz.txt", "w");
        if(fajl == NULL)
        {
            printf("Greska pri otvaranju fajla!\n");
            return 1;
        }
        char buf;
        while(read(pd[0], &buf, sizeof(char)) > 0)
        {
            if(buf == 'b')
            {
                buf = 'B';
            }
            fprintf(fajl, "%c", buf);
        }
        wait(NULL);
        close(pd[0]);
        fclose(fajl);
    }
    return 0;
}
