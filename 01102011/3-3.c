/*
 *Korišćenjem programskog jezika C kreirati dva Linux procesa koji komuniciraju korišćenjem
 *mehanizma redova poruka (message queues). Prvi proces čita stringove sa tastature i upisuje ih u red poruka
 *sve dok se na tastaturi ne unese reč KRAJ. Drugi proces čita stringove iz reda poruka (dok ne detektuje string
 *KRAJ) i prikazuje ih na standardnom izlazu. U sistemu postoji i treći proces koji kreira red poruka i pokreće ova
 *dva procesa (execv) a zatim čeka da se oni završe i tek nakon toga briše red poruka i završava svoje izvršavanje.
 */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <wait.h>

#define RED_PORUKA 10003
#define MAX_PORUKA 255

int main(int argc, char *argv[])
{
    int msqid;
    msqid = msgget(RED_PORUKA, IPC_CREAT | 0666); //ovaj proces kreira red poruka
    char *argumenti[2]; //niz argumenata za funkciju execv
    argumenti[0] = malloc(10 * sizeof(char)); //prvi argument ce sadrzati ime programa koji pozivamo
    argumenti[1] = NULL; // drugi argument je NULL, oznacava kraj niza argumenata
    if(fork() == 0)
    {
        strcpy(argumenti[0], "./3-1");
        if(execv("./3-1", argumenti) < 0)
        {
            printf("Greska pri izvrsavanju prvog\n");
            exit(1);
        }
    }
    if(fork() == 0)
    {
        strcpy(argumenti[0], "./3-2");
        if(execv("./3-2", argumenti) < 0)
        {
            printf("Greska pri izvrsavanju drugog");
            exit(1);
        }
    }
    
    free(argumenti[0]);
    wait(NULL);
    msgctl(msqid, IPC_RMID, NULL); //brisemo red poruka
    return 0;
}

