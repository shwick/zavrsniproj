/*
 *Korišćenjem programskog jezika C kreirati dva Linux procesa koji komuniciraju korišćenjem
 *mehanizma redova poruka (message queues). Prvi proces čita stringove sa tastature i upisuje ih u red poruka
 *sve dok se na tastaturi ne unese reč KRAJ. Drugi proces čita stringove iz reda poruka (dok ne detektuje string
 *KRAJ) i prikazuje ih na standardnom izlazu. U sistemu postoji i treći proces koji kreira red poruka i pokreće ova
 *dva procesa (execv) a zatim čeka da se oni završe i tek nakon toga briše red poruka i završava svoje izvršavanje.
 */
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

#define RED_PORUKA 10003
#define MAX_PORUKA 255

struct poruka
{
    long tip;
    char text[MAX_PORUKA];
};

int main(int argc, char *argv[])
{
    int msqid;
    struct poruka bafer;
    bafer.tip = 1;
    msqid = msgget(RED_PORUKA, 0666); //ne kreiramo ga - nema IPC_CREAT
    if(msqid < 0)
    {
        printf("Prvi proces greska reda poruka\n");
        exit(1);
    }
    while(strcmp(bafer.text, "KRAJ") != 0)
    {
        // uzimamo unos sa tastature, saljemo u red poruka
        scanf("%s", bafer.text);
        msgsnd(msqid, &bafer, MAX_PORUKA, 0);
    }
    return 0;
}
