/*
 *Korišćenjem programskog jezika C napisati Linux program koji omogućava modifikaciju standardnog
 *izlaza bilo kog programa. Naziv programa koji se startuje i čiji se izlaz modifikuje se zadaje kao argument
 *komandne linije. Standardni izlaz se modifikuje tako da se svi karakteri konvertuju u odgovarajuće male
 *karaktere pre nego što se odštampaju na standardnom izlazu.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
    int i;
    int pd[2];
    if(pipe(pd) < 0)
    {
        printf("Greska pri kreiranju pipea!\n");
        return 1;
    }
    if(fork() == 0)
    {
        close(pd[0]);
        dup2(pd[1], 1); //redirekcija izlaza
        //dup2(pd[1], STDOUT_FILENO); //moze i ovako, STDIN_FILENO bi bio ulaz

        execvp(argv[1], &argv[1]);
    }
    else
    {
        close(pd[1]);
        char buf;
        while(read(pd[0], &buf, sizeof(char)) > 0)
        {
            buf = tolower(buf);
            printf("%c", buf);
        }
        wait(NULL);
        close(pd[0]);
    }
    return 0;
}
