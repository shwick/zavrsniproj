/*
 *Korišćenjem programskog jezika C napisati Linux program koji simulira problem
 *proizvođač/potrošač korišćenjem redova poruka (message-queues). Glavni program se deli u dva procesa. Prvi
 *proces (proizvođač) kreira N slučajnih pozitivnih celih brojeva i šalje ih drugom procesu. N se određuje tokom
 *izvršenja, takođe kao slučajan pozitivan ceo broj. Po završetku slanja, prvi proces šalje -1 kao kod za kraj. Drugi
 *proces (potrošač) preuzima poslate brojeve iz poruka i štampa ih na standardnom izlazu.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define RED_PORUKA 10001
#define MAX_PORUKA 10

struct poruka
{
    long tip;
    char tekst[MAX_PORUKA];
};

int main(int argc, char *argv[])
{
    int redid;
    int pid;
    struct poruka bafer;

    if(fork() != 0)
    {
        int i;
        int N;
        redid = msgget(RED_PORUKA, IPC_CREAT | 0666);
        if(redid < 0)
        {
            printf("Greska kod reda procesa - proizvodjac\n");
            exit(1);
        }
        srand(2304);
        N = rand() % 20;
        for(i = 0; i < N; i++)
        {
            bafer.tip = 1;
            if(i == N)
            {
                sprintf(bafer.tekst, "%d", -1);
            }
            else
            {
                sprintf(bafer.tekst, "%d", rand() % 100);
            }
            if(msgsnd(redid, &bafer, sizeof(bafer.tekst), 0) < -1)
            {
                printf("Greska pri slanju poruke - proizvodjac\n");
                exit(1);
            }
        }
    }
    else
    {
        int primljeniBroj;
        redid = msgget(RED_PORUKA, IPC_CREAT | 0666);
        if(redid < 0)
        {
            printf("Greska kod reda poruka - potrosac\n");
            exit(1);
        }
        do
        {
            if(msgrcv(redid, &bafer, MAX_PORUKA, 0, 0) < -1)
            {
                printf("Greska kod prijema poruke - potrosac\n");
                exit(1);
            }
            primljeniBroj = atoi(bafer.tekst);
            printf("%d\n", primljeniBroj);

        }while(primljeniBroj != -1);

    }


    return 0;
}
