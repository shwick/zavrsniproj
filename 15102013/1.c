/*
 * Korišćenjem programskog jezika C napisati Linux program koji na standardnom izlazu ispisuje
 *rečenicu “Ispit iz Operativnih sistema” Pri čemu svaku reč u rečenici ispisuje posebna nit. Niti sinhronizovati
 *korišćenjem semafora.
 */
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

//4 reci ne duzih od 15 karaktera
char recenica[4][15] = {"Ispit", "iz", "Operativnih", "sistema." };
sem_t semafori[3]; //trebace nam 3 semafora

struct argumentNiti
{
    int brReci;
    pthread_t prethodnaNit;
};

void* stampanje(void *arg)
{
    struct argumentNiti *argument = (struct argumentNiti*) arg;
    //prva nit odmah stampa rec, ostale moraju da cekaju
    if(argument->brReci > 0)
        sem_wait(&semafori[argument->brReci - 1]);

    printf("%s ", recenica[argument->brReci]);
    fflush(NULL);
    //otkljucavamo nit koja stampa sledecu rec
    sem_post(&semafori[argument->brReci]);
    /*sleep(3);*/
    return 0;
}

int main(int argc, char *argv[])
{
    int i;
    struct argumentNiti args[4];
    pthread_t niti[4];

    for(i = 0; i < 4; i++)
        sem_init(&semafori[i], 0, 0);

    for(i = 0; i < 4; i++)
    {
        args[i].brReci = i;
        if(i > 0)
        {
            args[i].prethodnaNit = niti[i - 1];
        }
        pthread_create(&niti[i], NULL, (void*)stampanje, (void*)&args[i]);
    }

    for(i = 0; i < 4; i++)
    {
        pthread_join(niti[i], NULL);
    }
    return 0;
}
