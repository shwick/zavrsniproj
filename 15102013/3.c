/*
 *Korišćenjem programskog jezika C napisati Unix/Linux program koji kao argument komandne linije
 *uzima naziv tekstualne datoteke u tekućem direktorijumu koja sadrži više linija teksta. Program se deli na dva
 *procesa pri čemu proces roditelj otvara tekstualnu datoteku i čita iz nje liniju po liniju teksta i korišćenjem
 *deljene memorije pročitanu liniju teksta prosleđuje procesu detetu. Proces dete štampa na standardnom izlazu
 *(monitoru) linije teksta koje prihvata iz deljene memorije. Kako bi obezbedili da proces dete štampa linije
 *teksta onim redosledom kako se javljaju u tekstualnoj datoteci sinhronizovati procese korišćenjem IPC System
 *V semafora.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <fcntl.h>
#include <wait.h>
#include <stdbool.h>

#define MEM_KEY 10001
#define PROC_A_KEY 10011
#define PROC_B_KEY 10012
#define BUF_SIZE 100

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
};

struct readStruct
{
    bool done;
    char buf[BUF_SIZE];
};

int main(int argc, char *argv[])
{
    int procaid, procbid;
    union semun semopts;
    int status;

    struct sembuf sem_lock = { 0, -1, 0 };
    struct sembuf sem_unlock = { 0, 1, 0 };

    procaid = semget((key_t)PROC_A_KEY, 1, 0666 | IPC_CREAT);
    procbid = semget((key_t)PROC_B_KEY, 1, 0666 | IPC_CREAT);

    semopts.val = 1;
    semctl(procaid, 0, SETVAL, semopts);
    semopts.val = 0;
    semctl(procbid, 0, SETVAL, semopts);

    if(fork() != 0)
    {
        int shmId = shmget(MEM_KEY, sizeof(struct readStruct), IPC_CREAT | 0666);
        struct readStruct *shmPtr = shmat(shmId, NULL, 0);
        FILE *fajl = fopen(argv[1], "r");
        if(fajl == NULL)
        {
            printf("Greska pri otvaranju fajla\n");
            exit(1);
        }
        while(!feof(fajl))
        {
            semop(procaid, &sem_lock, 1);
            fgets(shmPtr->buf, BUF_SIZE, fajl);
            shmPtr->done = false;
            /*printf("procitao %s\n", shmPtr->buf);*/
            semop(procbid, &sem_unlock, 1);
        }
        shmPtr->done = true;
        fflush(NULL);
        shmdt(shmPtr);

        wait(NULL);
        shmctl(shmId, IPC_RMID, NULL);
        semctl(procaid, IPC_RMID, 0);
        semctl(procbid, IPC_RMID, 0);
        fclose(fajl);
    }
    else
    {
        int shmId = shmget(MEM_KEY, sizeof(struct readStruct), IPC_CREAT | 0666);
        struct readStruct *shmPtr = shmat(shmId, NULL, 0);
        sleep(1);
        while(shmPtr->done != true)
        {
            semop(procbid, &sem_lock, 1);
            char tekst[BUF_SIZE];
            strcpy(tekst, shmPtr->buf);
            printf("%s",shmPtr->buf);
            semop(procaid, &sem_unlock, 1);
            fflush(stdout);
        }

        shmdt(shmPtr);
        fflush(stdout);
    }

    return 0;
}
